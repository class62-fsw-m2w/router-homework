# HomeWork

1. Use this base code to create one 2-page app.
2. Use react-router-dom library. 
3. click small image, then click the nav "single Image", page will jump to /selectedImage/:index.
4. Selected image will appear in this page.
5. Refresh the page, image is still there.
6. type index directly in the address bar, corresponding image will appear.